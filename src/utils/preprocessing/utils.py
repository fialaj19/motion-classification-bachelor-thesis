from os import listdir, path
from os.path import isfile, isdir
import re
from pathlib import Path



def deduplicate_delimiters(line: str, delimeter: str) -> str:
    """
    Shrinks multiple space characters in line to only one
    :param line: Input string containing duplicate delimiter in line
    :param delimeter: The delimiter
    :return: String without duplicate delimiters
    """
    return delimeter.join([word for word in line.split(delimeter) if not word == ''])


def split_by_multiple_delimiters(line: str, delimiters: list) -> list:
    """
    Splits string by multiple delimiters.
    Hello,there;mate  -->  ['Hello', 'there', 'mate'] for delimiters [',', ';']
    :param line: Line to be split
    :param delimiters: Delimiters
    :return: List of substrings, split by delimiters
    """
    if len(delimiters) == 0:
        return line
    # If there are multiple delimiters, replace all of them with the first delimiter
    elif len(delimiters) > 1:
        for delim in delimiters:
            line = line.replace(delim, delimiters[0])

    line = deduplicate_delimiters(line, delimiters[0])

    return line.split(delimiters[0])


def remove_non_numbers(string: str) -> str:
    """
    Removes from string every character, which is not number
    :param string: Input string containing numbers, letters etc.
    :return: Output string containing only number chars
    """
    new_str = ''
    for char in string:
        if char.isnumeric():
            new_str += char

    return new_str


class ParseFile:

    def __init__(self, reorder_data: dict, separator=','):
        """
        :param separator: Separator to separate columns in file
        :param reorder_data: Template to reorder incoming rows,
                             This dictionary must be:
                                    key -> integer, index of incoming column to keep
                                    value -> (string, integer), string is new column name and integer is index in the table
                             All columns, which dont have their in dictionary will be dropped
        """
        self.__separator__ = separator
        self.__first_row__ = True
        self.__row_len__ = -1
        self.__columns__ = list()
        self.__reorder_data__ = reorder_data

    def add_row(self, row: list, valid_len: int):
        """
        Appends a new row to the end of rows
        :param valid_len: Row will processed only if it has valid_len number of columns. Set it to -1 to accept every row
        :param row: A new row to append. Length must be the same as every row already appended
        """

        if valid_len >= 0 and valid_len != len(row):
            return

        if self.__first_row__:
            self.__first_row__ = False
            self.__row_len__ = len(row)
            for key in self.__reorder_data__.keys():
                self.__columns__.append([])

        if self.__row_len__ != len(row):
            raise IndexError('Appended row has invalid lenght !')

        # Iterate over dict to get information about which columns should keep in what order
        for item in self.__reorder_data__.items():
            input_column = item[0]
            value_tuple = item[1]
            output_column = value_tuple[1]

            column = self.__columns__[output_column]
            column.append(row[input_column])

    def __get_header__(self) -> str:
        header = ''
        reorder_data_list = []
        for pair in self.__reorder_data__.items():
            reorder_data_list.append(pair[1])

        reorder_data_list = sorted(reorder_data_list, key=(lambda elem: elem[1]))

        for i in range(len(reorder_data_list)):
            if i == (len(reorder_data_list) - 1):
                header = header + (reorder_data_list[i])[0]
            else:
                header = header + (reorder_data_list[i])[0] + self.__separator__

        return header

    def write_to_file(self, file_name_full_path: str):
        """
        Writes all rows to the file, overwriting any content already in the file
        :param file_name_full_path: Full path to the file
        """
        with open(file_name_full_path, 'w') as file_write:
            file_write.write(self.__get_header__() + '\n')
            row_count = len(self.__columns__[0])
            for i in range(row_count):
                line = ''
                for j in range(len(self.__columns__)):
                    column = self.__columns__[j]
                    if j == (len(self.__columns__) - 1):
                        line = line + column[i]
                    else:
                        line = line + column[i] + self.__separator__

                line = line + '\n'
                file_write.write(line)


class DataParser:

    def __init__(self, path_to_folder: str, output_folder_path: str, output_file_extension: str = 'csv'):
        """
        Initializes class with path to data folder

        :param output_file_extension: File extension of newly created files
        :param output_folder_path: Path to folder, where will appear the new files
        :param path_to_folder: Path to the folder containing data files.
                               Will be searched recursively (including all subfolders)
        """

        self.__skip_odd_even__ = False
        self.__skip_odd__ = False
        self.__row_valid_len__ = -1
        self.__output_extension__ = output_file_extension
        self.__output_folder__ = output_folder_path
        self.__skip_lines__ = 0
        self.__column_separator__ = list()
        self.__data_folder_path__ = path_to_folder
        self.__excluded_file_names__ = list()
        self.__included_file_types__ = list()
        self.__path_must_contain__ = list()
        self.__include_columns__ = dict()

        # Create output directory if needed
        Path(self.__output_folder__).mkdir(parents=True, exist_ok=True)

        self.__file_name_counter__ = self.__get_max_filename_number__() + 1

    def __get_max_filename_number__(self) -> int:
        """
        Finds max number contained in all filenames of output_folder folder
        :return: Max number in all filenames
        """
        max = 0
        for obj_name in listdir(self.__output_folder__):
            full_path = path.join(self.__output_folder__, obj_name)
            if not isfile(full_path):
                continue

            name_numbers_only = remove_non_numbers(obj_name)

            if len(name_numbers_only) == 0:
                continue
            curr_number = int(name_numbers_only)
            if curr_number > max:
                max = curr_number

        return max

    def exclude_file_name(self, file_name_regexp: str):
        """
        Add a new file name regular expression, which will potentially exclude many files from data collection

        :param file_name_regexp: Regular expression matching all excluded file names
        """
        self.__excluded_file_names__.append(file_name_regexp)

    def set_valid_row_len(self, valid_len: int):
        """
        Sets if line with invalid count of columns should be skipped or raise an exception
        :param valid_len: Rows of files will be processed only if the row has valid_len number of columns.
                          Set to -1 to accept any row
        """
        self.__row_valid_len__ = valid_len

    def include_file_type_extension(self, file_extension: str):
        """

        :param file_extension: Files with this extension will be searched. If you dont add any extensions,
                               no files will be searched
        """
        self.__included_file_types__.append(file_extension)

    def path_to_file_must_contain(self, folder_name: str):
        """
        File will be searched only if path to it contains folder with name containing provided string

        :param folder_name: String which must be contained in a folder name
        """
        self.__path_must_contain__.append(folder_name)

    def skip_lines(self, skip_lines_cnt: int):
        """

        :param skip_lines_cnt: Number of skipped lines in each seached file
        """
        self.__skip_lines__ = skip_lines_cnt

    def skip_every_odd_or_even_line(self, skip_odd_even: bool, odd: bool):
        """
        Sets if every other line should be skipped, if yes then, every odd or every even.
        Keep in mind that line are counted from 1 !
        :param skip_odd_even: Should this feature be active
        :param odd: True -> skips every odd line, False -> skips every even line
        """
        self.__skip_odd_even__ = skip_odd_even
        self.__skip_odd__ = odd

    def add_separator(self, separator: str):
        """
        Add column separator to separate columns in each file

        :param separator: Column separator
        """
        self.__column_separator__.append(separator)

    def include_column(self, column_index: int, column_name: str, target_index: int):
        """
        Specifies, which columns should be extracted from files

        :param target_index: Column's index in the newly created file
        :param column_index: Column's index in the original file
        :param column_name: New name in generate table for this column
        """
        if column_index < 0:
            raise IndexError('Column index can not be lower than 0 !')

        if column_index in self.__include_columns__:
            raise IndexError('This column is already included !')

        self.__include_columns__[column_index] = (column_name, target_index)

    def run_parse(self):
        """
        Starts recursive search in folder provided in init
        """
        unmet_folder_names = frozenset(self.__path_must_contain__)
        self.__parse_recursion__(self.__data_folder_path__, unmet_folder_names)

    def __parse_file__(self, full_path: str):
        """
        Parses table data in file and writes result to file specified by self.__file_name_counter__ (from 0 to N)
        :param full_path: Path to file to be parsed
        """
        line_counter = 0

        table_file = ParseFile(self.__include_columns__)

        print('Processing file ' + full_path)

        with open(full_path, 'r') as file_read:
            while True:
                curr_line = file_read.readline()
                line_counter += 1

                # End of file
                if not curr_line:
                    break

                # Skip first self.__skip_lines__ lines
                if line_counter <= self.__skip_lines__:
                    continue

                # If skip-every-other-line is active, run the logic
                if self.__skip_odd_even__:
                    if self.__skip_odd__ and line_counter % 2 == 1:
                        continue
                    elif line_counter % 2 == 0:
                        continue

                curr_line = curr_line.replace('\n', '')

                # Pass split row to storage object
                table_file.add_row(split_by_multiple_delimiters(curr_line, self.__column_separator__), self.__row_valid_len__)

        # Write data from storage object to file
        table_file.write_to_file(
            path.join(self.__output_folder__, str(self.__file_name_counter__) + '.' + self.__output_extension__))
        self.__file_name_counter__ += 1

    def __parse_recursion__(self, curr_path: str, unmet_folder_names_along_path: frozenset):
        """
        Recursively searches all folders and subfolders for files matching all criteria, then calls parse_file if matches
        :param path: Path to folder to search
        :param unmet_folder_names_along_path: Folders names which must be in path for files to be searched
        """
        # Search through all objects in directory (folders + files mixed)
        for obj_name in listdir(curr_path):
            full_path = path.join(curr_path, obj_name)
            if isfile(full_path):
                # If there are unmet limitations on path name, dont search the file
                if len(unmet_folder_names_along_path) > 0:
                    continue

                # Check if filetype extension matches any allowed file types
                if obj_name.split('.')[-1] not in self.__included_file_types__:
                    continue

                # Try if the filename matches any forbidden filename regex
                excluded = False
                for excluded_re in self.__excluded_file_names__:
                    if re.match(excluded_re, obj_name):
                        excluded = True
                        break

                # You cant continue outer loop directly in Python :(
                if excluded:
                    continue

                # This file passed all limitation, run parser on it
                self.__parse_file__(full_path)

            elif isdir(full_path):

                checked = False
                # If this is as directory, call recursion again
                for unmet_name in unmet_folder_names_along_path:
                    if unmet_name in obj_name:

                        tmp = set(unmet_folder_names_along_path)
                        tmp.discard(unmet_name)
                        new_set = frozenset(tmp)

                        self.__parse_recursion__(full_path, new_set)
                        checked = True
                        break
                if not checked:
                    self.__parse_recursion__(full_path, unmet_folder_names_along_path)
