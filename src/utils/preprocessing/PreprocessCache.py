import glob
import hashlib
import pickle
from enum import Enum

import pandas as pd
from frozendict import frozendict
from utils.preprocessing.preprocess_utils import DatasetSplitter, generate_column_names, get_activity_map


class DatasetType(Enum):
    PAMAP = "PAMAP"


class PreprocessCache:
    def __init__(self):
        self.__cache_path = "."
        self.__cache = self.__load_cache_from_disk()
        self.__save_after_every_preprocess = True
        print("Loaded !")

    def __load_cache_from_disk(self):
        try:
            # Load data (deserialize)
            with open('../preprocessed_datasets/preprocessed_datasets.pickle', 'rb') as handle:
                print("Starting to load dataset cache from disk")
                return pickle.load(handle)
        except FileNotFoundError:
            return dict()

    def __load_pamap_dataset(self, time_window: float, overlap: float, keep_actitvities):
        splitter = DatasetSplitter('activity_id', 'timestamp (s)')

        for path in glob.glob('../training_data/PAMAP2_Dataset/*/*.dat'):
            print(f"Reading {path}")
            temp_df = pd.read_csv(path, sep=' ', names=generate_column_names())
            splitter.process_data_frame(temp_df)

        print("Loaded data from disk")

        splitter.split_by_time_window(time_window, overlap, keep_actitvities)

        result = splitter.get_splitted_result()

        for key in result:
            activity_name = get_activity_map()[key]
            frames_list = result[key]
            print(f'Move {activity_name} has {str(len(frames_list))} recordings !')

        return result

    def __save_cache(self):
        with open('../preprocessed_datasets/preprocessed_datasets.pickle', 'wb') as handle:
            pickle.dump(self.__cache, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def __read_and_process_dataset(self, dataset_type: DatasetType, time_window: float, overlap: float, keep_activities):
        if dataset_type == DatasetType.PAMAP:
            return self.__load_pamap_dataset(time_window, overlap, keep_activities)
        else:
            raise ValueError("Invalid dataset type")

    def __get_filtered_activities(self, dataset_type: DatasetType, dataset, activity_names: tuple):
        """
        Returns preprocessed data only for activities specified in argument
        :param dataset_type: Dataset type. Only PAMAP2 supported currently
        :param dataset: The dataset in dictionary format
        :param activity_names: Names of filtered activities
        :return: Filtered dataset
        """
        # Make deep copy
        dataset = dict(dataset)

        ids_to_remove = []

        if dataset_type == DatasetType.PAMAP:
            for activity_id, data in dataset.items():
                if get_activity_map()[activity_id] not in activity_names:
                    ids_to_remove.append(activity_id)
            for id in ids_to_remove:
                dataset.pop(id, None)
            return dataset
        else:
            raise ValueError("Invalid dataset type")

    def __save_dataset(self, dataset_hash: str, dataset):
        with open(f'../preprocessed_datasets/dataset_{dataset_hash}.pickle', 'wb') as handle:
            pickle.dump(dataset, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def __load_dataset(self, dataset_hash: str):
        with open(f'../preprocessed_datasets/dataset_{dataset_hash}.pickle', 'rb') as handle:
            return pickle.load(handle)

    def __get_dataset_hash(self, params: frozendict):
        infostr = ",".join([
            params.get("dataset_type"),
            str(params.get("time_window")),
            str(params.get("overlap")),
        ])

        return infostr

    def get_dataset(self, dataset_type: DatasetType, time_window: float, overlap: float, activity_names: tuple = None):
        params = frozendict({
            "dataset_type": dataset_type.value,
            "time_window": time_window,
            "overlap": overlap,
        })

        if not self.__cache.get(params):
            dataset = self.__read_and_process_dataset(dataset_type, time_window, overlap, activity_names)
            obj_hash = self.__get_dataset_hash(params)

            self.__cache[params] = obj_hash
            self.__save_cache()
            self.__save_dataset(obj_hash, dataset)
        else:
            obj_hash = self.__get_dataset_hash(params)
            dataset = self.__load_dataset(obj_hash)

        if activity_names:
            dataset = self.__get_filtered_activities(dataset_type, dataset, activity_names)

        reverse_rename_dict = {name: id for name, id in get_activity_map().items()}

        return {reverse_rename_dict[int(id)]: val for id, val in dataset.items()}

