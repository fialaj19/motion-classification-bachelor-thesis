from math import floor

import pandas as pd


def generate_column_names() -> list:
    """
    :return: list of column names for PAMAP2 dataset
    """
    column_names = ['timestamp (s)', 'activity_id', 'heart_rate (bpm)']
    column_names.extend(generate_imu('hand'))
    column_names.extend(generate_imu('chest'))
    column_names.extend(generate_imu('ankle'))
    return column_names


def generate_imu(imu_name: str) -> list:
    """
    :param imu_name: Name of the IMU
    :return: columns names for PAMAP2 dataset for one IMU
    """
    column_names = [imu_name + '_temperature (°C)']
    column_names.extend(generate_xyz(imu_name + '_acc_high_range'))
    column_names.extend(generate_xyz(imu_name + '_acc_low_range'))
    column_names.extend(generate_xyz(imu_name + '_gyro'))
    column_names.extend(generate_xyz(imu_name + '_magnet'))
    column_names.extend(generate_xyzw(imu_name + '_invalid_orient'))
    return column_names


def generate_xyz(name: str) -> list:
    column_names = [name + '_x', name + '_y', name + '_z']
    return column_names


def generate_xyzw(name: str) -> list:
    column_names = [name + '_x', name + '_y', name + '_z', name + '_w']
    return column_names


def get_activity_map() -> dict:
    activities = {
        0: 'transient',
        1: 'lying',
        2: 'sitting',
        3: 'standing',
        4: 'walking',
        5: 'running',
        6: 'cycling',
        7: 'Nordic_walking',
        9: 'watching_TV',
        10: 'computer_work',
        11: 'car driving',
        12: 'ascending_stairs',
        13: 'descending_stairs',
        16: 'vacuum_cleaning',
        17: 'ironing',
        18: 'folding_laundry',
        19: 'house_cleaning',
        20: 'playing_soccer',
        24: 'rope_jumping',
    }
    return activities


class DatasetSplitter:

    def __init__(self, activity_id_col: str, time_stamp_col: str):
        self.__activity_dict = {}
        self.__activity_id_col = activity_id_col
        self.__time_stamp_col = time_stamp_col

    def __preprocess_dataframe(self, df: pd.DataFrame):
        """
        Preprocessing stored dataframe.
        Currently resets the timestamp to start from zero for each dataframe
        :param df: Input dataframe
        """
        if self.__time_stamp_col is not None:
            df[self.__time_stamp_col] = df[self.__time_stamp_col] - df.iloc[0][self.__time_stamp_col]

    def __add_dataframe_to_dict(self, named_tuples_list: list, column_names: list):
        """
        Adds dataframe to activity dict, where keys are activity_ids and value are lists of dataframes
        :param df: List of rows
        """
        df = pd.DataFrame.from_records(named_tuples_list, columns=column_names)
        self.__preprocess_dataframe(df)
        activity_id = df.iloc[0][self.__activity_id_col]
        if activity_id in self.__activity_dict:
            self.__activity_dict[activity_id].append(df)
        else:
            self.__activity_dict[activity_id] = [df]

    def __calculate_activities_lenghts(self, keep_activities: tuple):
        """
        Calculates total recording lenght for each activity in dataset
        :param keep_activities: Only provided activities will have the lenghts calculated
        :return: dictionary containing lenght of each recording in seconds
        """
        lenghts_dict = {}

        for activity_id, recordings in self.__activity_dict.items():
            if get_activity_map()[activity_id] not in [activity_name for activity_name in keep_activities]:
                continue
            for recording in recordings:
                recording_len = recording.iloc[-1][self.__time_stamp_col]

                if activity_id in lenghts_dict:
                    lenghts_dict[activity_id] += recording_len
                else:
                    lenghts_dict[activity_id] = recording_len

        return lenghts_dict

    def process_data_frame(self, df: pd.DataFrame):
        """
        Split dataframe by activities and stores every activity recording
        :param df: Input dataframe containing many activity recordings (can contain only one)
        """
        if len(df.index) < 1:
            return

        df.interpolate(inplace=True)

        curr_activity_id = -10
        column_list = []
        for row in df.itertuples(index=False):
            if curr_activity_id < 0:
                column_list = []
                curr_activity_id = getattr(row, self.__activity_id_col)

            if not curr_activity_id == getattr(row, self.__activity_id_col):
                self.__add_dataframe_to_dict(column_list, list(df.columns))
                column_list = []
                curr_activity_id = getattr(row, self.__activity_id_col)
            column_list.append(row)

    def __split_df_by_time_window(self, df: pd.DataFrame, time_window: float, overlap: float, lenghts_dict, activity_id: int) -> list:
        """
        Splits input dataframe into smaller dataframes of same lenght. Smaller dataframes will overlap if overlap is specified or dataset balancing is active
        :param df: Input dataframe to be split
        :param time_window: Desired lenght of each smaller dataframe
        :param overlap: Desired overlap of each smaller dataframe. Must be from interval <0;1)
        :param lenghts_dict: Dictionary containing recording lenght of each activity. Used for dataset balancing
        :param activity_id: Activity type of input dataframe
        :return: list of smaller dataframes
        """
        chunks = []
        curr_chunk_rows = []
        max_lenght = -1

        for act_id, val in lenghts_dict.items():
            if val > max_lenght:
                max_lenght = val

        overlap_multipliers_dict = {act_id: (1-overlap)*val/max_lenght for act_id, val in lenghts_dict.items()}
        tmp_timestamp_col_name = "timestamp"
        df.rename(columns={self.__time_stamp_col: tmp_timestamp_col_name}, inplace=True)
        curr_columns = df.columns
        curr_start_timestamp = None
        rows = [row for row in df.itertuples(index=False)]

        i = 0
        while i < len(rows):
            row = rows[i]

            time_stamp = getattr(row, tmp_timestamp_col_name)

            if not curr_start_timestamp:
                curr_start_timestamp = time_stamp

            # When current chunk contains enough rows (specified by time window) it will be put into one smaller dataframe
            if time_stamp - curr_start_timestamp > time_window:
                tmp_df = pd.DataFrame.from_records(curr_chunk_rows, columns=curr_columns)
                tmp_df.rename(columns={tmp_timestamp_col_name: self.__time_stamp_col}, inplace=True)
                tmp_df[self.__time_stamp_col] = tmp_df[self.__time_stamp_col] - tmp_df[self.__time_stamp_col][0]
                i -= floor(len(curr_chunk_rows) * (1 - overlap_multipliers_dict.get(activity_id, 1)))
                row = rows[i]
                time_stamp = getattr(row, tmp_timestamp_col_name)
                chunks.append(tmp_df)
                curr_chunk_rows = []
                curr_start_timestamp = time_stamp

            curr_chunk_rows.append(row)

            i += 1

        # remaining rows are dropped
        return chunks

    def split_by_time_window(self, time_window: float, overlap: float, keep_activities):
        """
        Splits all recordings for all activities into samesized chunks.
        Drops all recording chunks, which dont fit the chunk size -> (only the last part of recording)
        :param time_window: Time window lenght in seconds
        """

        new_activity_dict = dict()

        lenghts_dict = self.__calculate_activities_lenghts(keep_activities)

        act_cnt = 0
        for activity_id, recordings in self.__activity_dict.items():
            chunks = []
            print(f"Processing activity {activity_id} with {len(recordings)}. {act_cnt} of {len(self.__activity_dict.items())}")
            cnt = 0
            for recording in recordings:
                print(f"Processing {cnt} of {len(recordings)} !")
                tmp_chunks = self.__split_df_by_time_window(recording, time_window, overlap, lenghts_dict, activity_id)
                chunks.extend(tmp_chunks)
                cnt += 1
            new_activity_dict[activity_id] = chunks
            act_cnt += 1
        self.__activity_dict = new_activity_dict

    def get_splitted_result(self) -> dict:
        """
        Returns dict, where:
                    - key is activity_id
                    - value is list of pandas dataframes, where one dataframe is one continuos recording of given activity
        """
        return self.__activity_dict
