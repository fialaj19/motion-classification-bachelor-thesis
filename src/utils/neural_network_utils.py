import os
import sys
from enum import Enum
from typing import Tuple
import pandas as pd
from keras import models, layers
from keras.callbacks import ModelCheckpoint
from keras.optimizers import RMSprop
from keras.saving.legacy.save import load_model
from more_itertools import powerset
from numpy import argmax
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.model_selection import train_test_split
from utils.preprocessing.PreprocessCache import PreprocessCache, DatasetType

dataset_cache = PreprocessCache()

# Supported activity types
keep_activities = (
    "lying",
    "sitting",
    "standing",
    "walking",
    "running",
    "cycling",
    "Nordic_walking",
    "ascending_stairs",
    "descending_stairs",
    "playing_soccer",
    "rope_jumping"
)

def encode_many_activity_ids(ids: list):
    """
    Encodes list of activity names to list of ids. Each activity name must be in supported activities
    :param ids: Input list of acitivity names
    :return: List of activity ids
    """
    return [encode_activity_id(act_id) for act_id in ids]

def decode_many_activity_ids(ids: list):
    """
    Decodes list of activity ids to list of names.
    :param ids: List of activity ids
    :return: list of acitivity names
    """
    return [decode_activity_id(act_id) for act_id in ids]

def encode_activity_id(activity_name: str):
    """
    Encodes activity name to activity id
    :param activity_name: Activity name
    :return: activity id
    """
    try:
        return keep_activities.index(activity_name)
    except ValueError:
        return None

def decode_activity_id(id_val: int):
    """
    Decodes activity id to activity name
    :param activity_name: Activity id
    :return: activity name
    """
    if id_val is None:
        return None
    return keep_activities[id_val]

def train_valid_test_split(X, y, test_or_validation_size: float, test_size: float):
    """
    Splits X, y data into training, validation and test parts
    :param X: Input data as numpy ndarray. Shape = (n_datapoints, timestamp, features)
    :param y: True classes for each datapoint. Shape = (n_datapoints,)
    :param test_or_validation_size: Size of test and validation data as fraction
    :param test_size: Size of test dataset
    :return: Split dataset in form of dictionary
    """
    X_train, X_tmp, y_train, y_tmp = train_test_split(X, y, test_size=test_or_validation_size, random_state=0)

    X_validation, X_test, y_validation, y_test = train_test_split(X_tmp, y_tmp, test_size=test_size, random_state=0)

    return {
        "train": {
            "X": X_train,
            "y": y_train,
        },
        "validation": {
            "X": X_validation,
            "y": y_validation,
        },
        "test": {
            "X": X_test,
            "y": y_test,
        }
    }

def __repair_NN_input(input_samples: list) -> list:
    """
    Makes all ndarrays in provided list the same lenght. Neccesary for input into neural network
    :param input_samples: List of ndarrays
    :return: List of ndarrays, all with the same lenght
    """
    min_len = min([sample.shape[0] for sample in input_samples])

    return [sample[:min_len, ...] for sample in input_samples]

def to_probability_distribution(value: int, class_count: int):
    """
    Returns probability distribution for given class. Probability of given class will be 1.0 and all other classes will 0.0
    :param value: The class id
    :param class_count: Number all possible classes
    :return: The probability distribution
    """
    return np.array([1.0 if i == value else 0.0 for i in range(class_count)])

def to_probability_distribution_many(values: list, class_count: int):
    """
    Returns list of probability distributions from list of classes
    :param values: List of classes
    :param class_count: Number of all prossible classes
    :return: List of probability distributions
    """
    return [to_probability_distribution(val, class_count) for val in values]

def __to_xyz(name: str) -> list:
    """
    :param name: Name
    :return: Returns list by appending _x, _y and _z to provided string
    """
    return [f"{name}{suffix}" for suffix in ["_x", "_y", "_z"]]

def preprocess_to_NN(dataset: dict, test_or_validation_size: float, test_size: float, column_names: list):
    """
    Preprocessing dataset represented as list of pandas dataframes into one numpy ndarray, which can be directly fed into keras neural network
    :param dataset: Input dataset as dictionary containing lists of pandas dataframes
    :param test_or_validation_size: Size of test and validation data as fraction
    :param test_size: Size of test dataset
    :param column_names: Column names, which will be used in the neural network input. Order matters !
    :return: Dictionary containing numpy ndarray input data split into training, validation and test datasets
    """
    parsed_column_names = []
    for name in column_names:
        parsed_column_names.extend(__to_xyz(name))

    X_raw = []
    y_raw = []
    for activity_id, recordings in dataset.items():
        print(f"Actid {activity_id}")
        count = 0
        for recording in recordings:
            if count % 100 == 0:
                print(f"Count {count} / {len(recordings)}")
            count += 1
            extracted_recording = []
            for row in recording.itertuples():
                features = []
                for column_name in parsed_column_names:
                    features.append(getattr(row, column_name))
                extracted_recording.append(np.array(features, dtype=np.float32))
            X_raw.append(np.stack(extracted_recording))
            y_raw.append(activity_id)

    X = np.stack(__repair_NN_input(X_raw))
    y = np.array(y_raw, dtype=str)

    print(f"X shape is {X.shape}")

    return train_valid_test_split(X, y, test_or_validation_size, test_size)

def print_accuracy_graph(accuracy: list, val_accuracy: list, max_val_accuracy: float, used_column_names: list, time_window: float):
    """
    Displays graph of model training accuracy
    :param accuracy: List of train accuracies during training
    :param val_accuracy: List of validation accuracies during training
    :param max_val_accuracy: Best validation accuracy
    :param used_column_names: Dataset column names used during training
    :param time_window: Used time widndow (in seconds)
    """
    plt.plot(accuracy)
    plt.plot(val_accuracy)
    plt.title(f'Window {time_window} s, (best val accuracy = {"{:.2f}".format(max_val_accuracy)}) \n ({used_column_names})')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show(block=True)

def print_confusion_matrix(y_predicted: list, y_true: list, display_labels: list):
    """
    Displays confusion matrix of provided data
    :param y_predicted: List of predicted classes
    :param y_true: List of true classes
    :param display_labels: List of class labels
    """
    cm = confusion_matrix(y_true, y_predicted)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=display_labels)
    disp.plot(xticks_rotation="vertical")
    disp.figure_.tight_layout()
    disp.figure_.set_size_inches(9, 8)
    plt.show(block=True)

def proprocess_y_NN(y_data):
    """
    Transforms list of human readable acitivity names into list of probability distributions of encoded activity names
    :param y_data: list of human readable acitivity names
    :return: list of probability distributions of encoded activity names
    """
    return np.stack(to_probability_distribution_many(encode_many_activity_ids(y_data), len(keep_activities)))

def build_recurrent_NN_model(data_dict: dict, time_window: float, used_column_names: list, batch_size: int = None, training_epochs: int = None):
    """
    Builds and trains recurrent neural network classification model (bidirectional LSTM).
    Keeps only the one with the best validation accuracy.
    Prints accuracy during training graph.
    Finally prediction with trained model is ran on test dataset and results are displayed in confusion matrix
    :param data_dict: Preprocessed train, validation and test data
    :param time_window: Used time window (in seconds)
    :param used_column_names: Only these columns will be used as input data
    :return: Trained keras model
    """
    X_train = data_dict["train"]["X"]
    y_train = proprocess_y_NN(data_dict["train"]["y"])

    X_validation = data_dict["validation"]["X"]
    y_validation = proprocess_y_NN(data_dict["validation"]["y"])

    model = models.Sequential()
    model.add(layers.Input(X_train.shape[1:]))
    model.add(layers.Bidirectional(layers.LSTM(64, dropout=0.3, recurrent_dropout=0.3, return_sequences=True)))
    model.add(layers.Bidirectional(layers.LSTM(64, dropout=0.3, recurrent_dropout=0.3, return_sequences=True)))
    model.add(layers.Bidirectional(layers.LSTM(64, dropout=0.3, recurrent_dropout=0.3)))
    model.add(layers.Dense(100, activation="relu"))
    model.add(layers.Dense(len(keep_activities), activation="softmax"))
    model.compile(optimizer=RMSprop(lr=0.0005), loss="categorical_crossentropy", metrics=["accuracy"])

    model.summary()

    best_model_path = "../trained_models/tmp_model.hdf5"
    save_checkpoint = ModelCheckpoint(best_model_path, monitor='val_loss', verbose=2,
                                      save_best_only=True, save_weights_only=False,
                                      mode='auto', period=1)

    history = model.fit(X_train, y_train, epochs=training_epochs or 50, batch_size=batch_size or 150, validation_data=(X_validation, y_validation), callbacks=[save_checkpoint])

    model = load_model(best_model_path)

    X_test = data_dict["test"]["X"]
    y_test = proprocess_y_NN(data_dict["test"]["y"])

    y_test_predict = model.predict(X_test)

    y_test_prep = probability_distribution_to_most_likely_class(y_test.tolist())
    y_test_predict_prep = probability_distribution_to_most_likely_class(y_test_predict)
    print_confusion_matrix(y_test_predict_prep, y_test_prep, list(keep_activities))

    print_accuracy_graph(history.history['accuracy'], history.history['val_accuracy'], max(history.history['val_accuracy']), used_column_names, time_window)

    return model

def probability_distribution_to_most_likely_class(distributions: list) -> list:
    """
    :param distributions: Input probability distribution
    :return: Class with the highest probability
    """
    res = []
    for distribution in distributions:
        res.append(argmax(distribution))
    return res

class ModelType(Enum):
    CNN = "CNN"
    LSTM = "LSTM"

    @classmethod
    def from_str(cls, value: str):
        for type in ModelType:
            if type.value == value:
                return type
        raise ValueError("Unknown type")

def build_NN_model(data_dict: dict, time_window: float, used_column_names: list, batch_size: int = None, training_epochs: int = None):
    """
    Builds and trains neural network classification model (convolutional neural network).
    Keeps only the one with the best validation accuracy.
    Prints accuracy during training graph.
    Finally prediction with trained model is ran on test dataset and results are displayed in confusion matrix
    :param data_dict: Preprocessed train, validation and test data
    :param time_window: Used time window (in seconds)
    :param used_column_names: Only these columns will be used as input data
    :return: Trained keras model
    """
    X_train = data_dict["train"]["X"]
    y_train = proprocess_y_NN(data_dict["train"]["y"])

    X_validation = data_dict["validation"]["X"]
    y_validation = proprocess_y_NN(data_dict["validation"]["y"])


    model = models.Sequential()
    model.add(layers.Input(X_train.shape[1:]))
    model.add(layers.Conv1D(128, 20, activation="relu"))
    model.add(layers.MaxPool1D(10))
    model.add(layers.Dropout(0.5))
    model.add(layers.Conv1D(64, 10, activation="relu"))
    model.add(layers.MaxPool1D(10))
    model.add(layers.Dropout(0.5))
    model.add(layers.Flatten())
    model.add(layers.Dense(300 * int(time_window / 5), activation="relu"))
    model.add(layers.Dense(len(keep_activities), activation="softmax"))

    model.compile(optimizer=RMSprop(lr=0.0003), loss="categorical_crossentropy", metrics=["accuracy"])

    model.summary()

    best_model_path = "../trained_models/tmp_model.hdf5"
    save_checkpoint = ModelCheckpoint(best_model_path, monitor='val_loss', verbose=2,
                                      save_best_only=True, save_weights_only=False,
                                      mode='auto', period=1)

    history = model.fit(X_train, y_train, epochs=training_epochs or 50, batch_size=batch_size or 200, validation_data=(X_validation, y_validation), callbacks=[save_checkpoint])

    model = load_model(best_model_path)

    X_test = data_dict["test"]["X"]
    y_test = proprocess_y_NN(data_dict["test"]["y"])

    y_test_predict = model.predict(X_test)

    y_test_prep = probability_distribution_to_most_likely_class(y_test.tolist())
    y_test_predict_prep = probability_distribution_to_most_likely_class(y_test_predict)
    print_confusion_matrix(y_test_predict_prep, y_test_prep, list(keep_activities))

    print_accuracy_graph(history.history['accuracy'], history.history['val_accuracy'], max(history.history['val_accuracy']), used_column_names, time_window)

    return model

# Possible type: ["hand", "chest", "ankle"]
def generate_column_names(types: list):
    """
    Generated all possible subset of probided strings with gyro and accelerator suffixes
    :param types: Sensor names
    :return: All subsets
    """
    subsets_prefixes = powerset(types)

    subsets_prefixes = list(subsets_prefixes)[1:]

    subsets = []
    for subsets_prefix in subsets_prefixes:
        subsets_prefix = list(subsets_prefix)
        tmp_list = []
        for column_name in subsets_prefix:
            tmp_list.extend([f"{column_name}{suffix}" for suffix in ["_acc_high_range", "_gyro"]])
        subsets.append(tmp_list)
    return subsets

def run_model_training(dataset_type: DatasetType, model_type: ModelType, time_window: float, overlap: float, keep_activities: tuple, used_column_names: list, batch_size: int = None, training_epochs: int = None):
    """
    Runs model training with provided params and returns trained model
    :return: Trained model
    """
    dataset = dataset_cache.get_dataset(dataset_type,
                                        time_window,
                                        overlap,
                                        activity_names=keep_activities,
                                        )
    data = preprocess_to_NN(dataset, test_or_validation_size=0.4, test_size=0.5, column_names=used_column_names)

    model = None

    if model_type == ModelType.CNN:
        model = build_NN_model(data, time_window, used_column_names=used_column_names, training_epochs=training_epochs, batch_size=batch_size)
    elif model_type == ModelType.LSTM:
        model = build_recurrent_NN_model(data, time_window, used_column_names=used_column_names, training_epochs=training_epochs, batch_size=batch_size)
    return model

def get_model_foldername(model_type: ModelType, time_window: float, overlap: float, sensors: list):
    """
    Generates folder name based on model parameters
    :return: Folder name
    """
    sensors_str = ",".join(sensors)
    return f"{model_type.value}_{time_window}_{overlap}_{sensors_str}"

def save_model_to_folder(model, foldername: str):
    """
    Saves trained model to a folder
    :param model: Trained model
    :param foldername: Folder name
    """
    path = f'../trained_models/{foldername}'
    if not os.path.exists(path):
        os.makedirs(path)
    model.save(path)

def load_model_from_folder(foldername: str):
    """
    Loads trained model from folder
    :param foldername: Folder name
    :return: Trained model
    """
    path = f'../trained_models/{foldername}'
    try:
        model = models.load_model(path)
    except IOError:
        print("Cant find model with selected settings. Please train one before running this demo")
        sys.exit(1)
    return model

def run_training(time_window: float, sensor_locations: list, overlap: float, modeltype: ModelType, batch_size: int = None, training_epochs: int = None):
    """
    Trains model with provided params and saves it to a folder
    """
    subset = generate_column_names(sensor_locations)[-1]

    model = run_model_training(DatasetType.PAMAP, modeltype, time_window, overlap, keep_activities, subset, batch_size=batch_size, training_epochs=training_epochs)
    folder_name = get_model_foldername(modeltype, time_window, overlap, subset)
    save_model_to_folder(model, folder_name)

def load_and_preprocess_demo_data(path: str, reordering: list, class_column: str, time_window: float, activity_resolver: dict = None) -> Tuple[list, list]:
    """
    Loads and preprocesses demo data
    :param path: Path to the demo dataset. Must csv type file
    :param reordering: Which column to use for demo classification
    :param class_column: Which column contains current activity type
    :param activity_resolver: Dictionary to transform arbitary activity name into activity name understood by this program
    :return: Iterator of preprocessed list of rows and actual movement types for each row
    """
    frame_chunks = pd.read_csv(path, sep=' ', header=None, chunksize=int(100*time_window))

    for chunk in frame_chunks:
        recording = []
        true_classes = []
        chunk.interpolate(inplace=True)
        for index, row in chunk.iterrows():
            features = []
            for column_name in reordering:
                features.append(row[column_name])
            recording.append(np.array(features, dtype=np.float32))
            act_id_readable = row[class_column]
            if activity_resolver:
                act_id_readable = activity_resolver[act_id_readable] if activity_resolver.get(act_id_readable) else None
            true_classes.append(encode_activity_id(act_id_readable))
        yield recording, true_classes

def find_most_frequent_elem(elem_list):
    """
    Finds the most frequent elem in list
    :return: The most frequent element
    """
    reduce_dict = {}

    for elem in elem_list:
        if elem is None:
            return None
        if elem in reduce_dict:
            reduce_dict[elem] += 1
        else:
            reduce_dict[elem] = 1

    max_count = -1
    max_elem = None
    for key, val in reduce_dict.items():
        if val > max_count:
            max_elem = key
            max_count = val

    return max_elem

def run_demo(time_window: float, overlap: float, demo_data_path: str, reordering: list, used_sensors: list, true_class_column: str, model_type: ModelType, activity_resolver: dict = None, run_as_generator: bool = False):
    """
    Runs demo classification with provided parameters
    """

    trained_model = load_model_from_folder(get_model_foldername(model_type, overlap=overlap, sensors=used_sensors, time_window=time_window))

    predicted_list = []
    true_list = []

    chunk_size = int(time_window*100)

    correct_predictions = 0
    total_predictions = 0
    for i, (rows, true_classes) in enumerate(load_and_preprocess_demo_data(path=demo_data_path, reordering=reordering, class_column=true_class_column, activity_resolver=activity_resolver, time_window=time_window)):
        true_class = find_most_frequent_elem(true_classes)
        if true_class is None:
            print(f"Skipping demo classification, because of unsupported movement type for current time window")
            continue

        true_list.append(true_class)
        input = np.stack(rows)
        print(f"Input shape is {input.shape}")
        predicted_prob_dist = trained_model.predict(np.stack([input]))
        predicted_class = probability_distribution_to_most_likely_class(predicted_prob_dist)[0]
        predicted_list.append(predicted_class)

        if true_class == predicted_class:
            correct_predictions += 1

        total_predictions += 1

        print(f"Demo from {i*chunk_size} to {(i + 1)*chunk_size}. Model predicted {decode_activity_id(predicted_class)} and ground truth is {decode_activity_id(true_class)}")

        if run_as_generator:
            graphs = {}
            sensors_xyz = []
            for sensor in used_sensors:
                sensors_xyz.extend(__to_xyz(sensor))
            for idx, name in enumerate(sensors_xyz):
                graphs[name] = input[:, idx]

            display_probabilities = {}
            for idx, probability in np.ndenumerate(predicted_prob_dist):
                display_probabilities[keep_activities[idx[1]]] = probability

            cm = confusion_matrix(decode_many_activity_ids(true_list), decode_many_activity_ids(predicted_list), labels=list(keep_activities))

            yield graphs, display_probabilities, decode_activity_id(true_class), decode_activity_id(predicted_class), correct_predictions / total_predictions, (time_window, i*chunk_size, (i + 1)*chunk_size), (cm, keep_activities)

    cm = confusion_matrix(decode_many_activity_ids(true_list), decode_many_activity_ids(predicted_list), labels=list(keep_activities))
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=list(keep_activities))
    disp.plot(xticks_rotation="vertical")
    disp.figure_.tight_layout()
    disp.figure_.set_size_inches(8, 8)
    plt.show()










