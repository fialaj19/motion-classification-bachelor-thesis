#!/bin/bash

time_windows=(5 10 20 30)
overlaps=(0.0 0.3 0.5)
sensor_combinations=("hand" "chest" "ankle" "hand,chest" "hand,ankle" "chest,ankle" "hand,chest,ankle")

for window in ${time_windows[@]}; do
  for overlap in ${overlaps[@]}; do
    for sensors in ${sensor_combinations[@]}; do
      python3 run_training.py --time-window=$window --model=CNN --overlap=$overlap --sensors=$sensors --batch-size=400 --epochs=50
    done;
  done;
done;

for window in ${time_windows[@]}; do
  for overlap in ${overlaps[@]}; do
    for sensors in ${sensor_combinations[@]}; do
      python3 run_training.py --time-window=$window --model=LSTM --overlap=$overlap --sensors=$sensors --batch-size=200 --epochs=20
    done;
  done;
done;
