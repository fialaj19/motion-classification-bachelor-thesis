import optparse
import sys

from utils.neural_network_utils import ModelType, run_training

if __name__ == "__main__":
    parser = optparse.OptionParser(description="Movement classifier training")
    parser.add_option("-m", "--model", dest="model_type", type=str, help="Model type. Only 'LSTM' or 'CNN' supported")
    parser.add_option("-o", "--overlap", dest="overlap", type=float,
                      help="Overlap of the time windows. Expressed in percentage of the time window -> valid values are in interval <0.0;1.0)")
    parser.add_option("-t", "--time-window", dest="time_window", type=float, help="Time window to split the training data by")
    parser.add_option("-s", "--sensors", dest="sensors", type=str,
                      help="Name of sensor placements separated by comma. Possible values are [\"hand\",\"ankle\",\"chest\"]")

    parser.add_option("-e", "--epochs", dest="epochs", type=int, help="Number of training epochs")
    parser.add_option("-b", "--batch-size", dest="batch_size", type=int, help="Size of training batch. Lower batch size means lower RAM usage")

    args, args2 = parser.parse_args()

    if not args.time_window:
        parser.print_help()
        print("Time window needs to be specified --time-window")
        sys.exit(1)

    if not args.model_type:
        parser.print_help()
        print("Model type needs to be specified with parameter --model")
        sys.exit(1)

    if args.overlap is None:
        parser.print_help()
        print("Overlap needs to be specified with parameter --overlap")
        sys.exit(1)

    if not args.sensors:
        parser.print_help()
        print("Sensors need to be specified with parameter --sensors")
        sys.exit(1)

    if not args.batch_size:
        parser.print_help()
        print("Batch size needs to be specified with parameter --batch-size")
        sys.exit(1)

    if not args.epochs:
        parser.print_help()
        print("Number of epochs needs to be specified with parameter --epochs")
        sys.exit(1)

    sensors = args.sensors.split(",") if args.sensors else None
    if sensors:
        for sensor in sensors:
            if sensor not in ["hand", "chest", "ankle"]:
                print("Invalid sensor type ! Must be one of: hand, chest, ankle")
                sys.exit(1)
    model_type = ModelType.from_str(args.model_type)
    run_training(args.time_window, sensors, args.overlap, model_type, training_epochs=args.epochs, batch_size=args.batch_size)