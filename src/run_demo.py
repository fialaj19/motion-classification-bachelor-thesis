import json
import optparse
import sys

from GUIDemo import GUIDemo
from utils.neural_network_utils import keep_activities, generate_column_names, ModelType, run_demo


def get_resolve_movement_dict(file_stream) -> dict:
    activities = json.load(file_stream)

    activity_dict = {}
    for activity_name, activity_id in activities.items():
        if activity_name not in keep_activities:
            raise ValueError(f"""Unknown activity name {activity_name} ! Must be one of {",".join(keep_activities)}""")

        act_id_number = int(activity_id)

        activity_dict[act_id_number] = activity_name

    return activity_dict

if __name__ == "__main__":
    parser = optparse.OptionParser(description="Movement classifier demo")
    parser.add_option("-w", "--time-window", dest="time_window", type=float, help="Time window to split the recording by (in seconds)")
    parser.add_option("-o", "--overlap", dest="overlap", type=float, help="Overlap of the time windows. Expressed in percentage of the time window -> valid values are in interval <0.0;1.0)")
    parser.add_option("-f", "--input-file", dest="input_file", type=str, help="Path to file containing csv data to run on demo")
    parser.add_option("-r", "--reordering", dest="reordering", type=str, help="Name of the columns extracted from the input file (order matters) separated by comma")
    parser.add_option("-s", "--sensors", dest="sensors", type=str, help="Name of sensor placements separated by comma. Possible values are [\"hand\",\"ankle\",\"chest\"]")
    parser.add_option("-m", "--movement-column", dest="activity_column", type=int, help="Name of the column containing true current activity")
    parser.add_option("-d", "--input-file-delimiter", dest="delimiter", type=str, help="Delimiter of the input csv file")
    parser.add_option("-l", "--model", dest="model_type", type=str, help="Model type. Only 'LSTM' or 'CNN' supported")

    (args, args2) = parser.parse_args()

    if not args.time_window:
        parser.print_help()
        print("Time window needs to be specified")
        sys.exit(1)

    if not args.input_file:
        parser.print_help()
        print("Input file needs to be specified")
        sys.exit(1)

    if not args.reordering:
        parser.print_help()
        print("Reordering needs to be specified")
        sys.exit(1)

    if not args.activity_column:
        parser.print_help()
        print("Activity column needs to be specified")
        sys.exit(1)

    if not args.model_type:
        parser.print_help()
        print("Model type needs to be specified")
        sys.exit(1)

    if not args.sensors:
        parser.print_help()
        print("Sensors need to be specified")
        sys.exit(1)

    sensors = generate_column_names(args.sensors.split(","))[-1] if args.sensors else None
    model_type = ModelType.from_str(args.model_type)
    with open("../demo_config.json") as f:
        movement_resolver = get_resolve_movement_dict(f)
    results = run_demo(time_window=args.time_window, overlap=args.overlap or 0.0, demo_data_path=args.input_file, reordering=[int(i) for i in args.reordering.split(",")], true_class_column=args.activity_column, used_sensors=sensors, model_type=model_type, activity_resolver=movement_resolver, run_as_generator=True)

    demo_display = GUIDemo(title="Movement classification demo - by Jan Fiala", window_width=1700, window_height=1000, sensor_names=args.sensors.split(","))
    demo_display.run_game_loop(results)