from typing import Dict, List
import pygame
import matplotlib
from sklearn.metrics import ConfusionMatrixDisplay

matplotlib.use("Agg")

import matplotlib.backends.backend_agg as agg
import pylab

FONT_FILENAME = pygame.font.get_default_font()

class GUIDemo:


    def __init__(self, title: str, window_width: int, window_height: int, sensor_names: str):
        self.__title = title
        self.__graph_size_inches = [3, 1]
        self.__graph_dpi = 150
        self.__sensor_names = sensor_names

        self.__progress_bar_size_pixels = [300, 30]
        self.__progress_bar_size_padding = [100, 80]
        self.__progress_bar_global_vertical_offset = 50
        self.__progress_bar_text_vertical_offset = 40

        self.__window_size = [window_width, window_height]

        pygame.display.set_caption(title)
        pygame.init()
        self.screen = pygame.display.set_mode(self.__window_size)

        self.screen.fill((255, 255, 255))
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"Please wait. Loading.....", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 200, self.__window_size[1] / 2))
        pygame.display.flip()

    def __get_graph_size_pixels(self):
        res = [size * self.__graph_dpi for size in self.__graph_size_inches]
        return res

    def __draw_graphs(self, graphs: Dict[str, List[float]]):
        for i, (name, values) in enumerate(graphs.items()):
            self.__draw_graph(name, values, vertical_offset=i * self.__get_graph_size_pixels()[1] + 20)
            self.__draw_text(20, i * self.__get_graph_size_pixels()[1], name)

    def __draw_info(self, time_window: float):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"Time window {time_window} seconds", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 20))

    def __draw_sensor_info(self):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"""Used sensors: {",".join(self.__sensor_names)}""", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 70))

    def __draw_timestamp(self, current_window_from: int, current_window_to: int, override_text: str = None):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"Current window (in seconds) from {current_window_from / 100} to {current_window_to / 100}", True, (0, 0, 0)) if not override_text else font.render(override_text, True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 120))

    def __draw_real_label(self, label: str):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"True label: {label}", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 170))

    def __draw_predicted_label(self, label: str):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(f"Predicted label: {label}", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 220))

    def __draw_classification_accuracy(self, accuracy: float):
        font = pygame.font.Font(FONT_FILENAME, 32)
        formatted_number = "{:.2f}".format(accuracy * 100)
        text = font.render(f"Classification accuracy {formatted_number} % so far", True, (0, 0, 0))
        self.screen.blit(text, (self.__window_size[0] / 2 - 350, 270))

    def __draw_text(self, start_width: int, start_height: int, text: str):
        font = pygame.font.Font(FONT_FILENAME, 32)
        text = font.render(text, True, (0, 0, 0))
        self.screen.blit(text, (start_width, start_height))

    def __draw_probability_dist(self, probability_dist: Dict[str, float]):
        for i, (name, value) in enumerate(probability_dist.items()):
            self.__draw_text(self.__get_progress_bar_location(index=i)[0], self.__get_progress_bar_location(index=i)[1] - self.__progress_bar_text_vertical_offset, name)
            self.__draw_percentage_bar(progress=1.0, color=(0, 255, 0), index=i)
            self.__draw_percentage_bar(progress=value, color=(255, 0, 0), index=i)
            formatted_number = "{:.2f}".format(value * 100)
            self.__draw_text(self.__get_progress_bar_location(i)[0] + self.__progress_bar_size_pixels[0] / 2, self.__get_progress_bar_location(i)[1], f"{formatted_number} %")

    def __draw_confusion_matrix(self, confusion_matrix, keep_activities):
        disp = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix,
                                      display_labels=list(keep_activities))
        disp.plot(xticks_rotation="vertical")
        disp.figure_.tight_layout()
        disp.figure_.set_size_inches(8, 8)
        canvas = agg.FigureCanvasAgg(disp.figure_)
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()
        size = canvas.get_width_height()
        surf = pygame.image.fromstring(raw_data, size, "RGB")
        self.screen.blit(surf, (self.__window_size[0] / 2 - 400, self.__window_size[1] / 2 - 200))

    def __get_progress_bar_location(self, index: int = 0):
        return (self.__window_size[0] - self.__progress_bar_size_padding[0] - self.__progress_bar_size_pixels[0], self.__progress_bar_global_vertical_offset + index * self.__progress_bar_size_padding[1])

    def __draw_percentage_bar(self, progress: float = 1.0, color: tuple = (0, 0, 255), index: int = 0):
        pygame.draw.rect(self.screen, color, pygame.Rect(self.__get_progress_bar_location(index)[0], self.__get_progress_bar_location(index)[1], self.__progress_bar_size_pixels[0] * progress, self.__progress_bar_size_pixels[1]))

    def __draw_graph(self, name: str, values: List[float], vertical_offset: int = 0):
        pylab.title(name)
        fig = pylab.figure(figsize=self.__graph_size_inches,
                           dpi=self.__graph_dpi,
                           layout="tight"
                           )
        ax = fig.gca()
        ax.clear()
        ax.plot(values)

        canvas = agg.FigureCanvasAgg(fig)
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()

        size = canvas.get_width_height()
        surf = pygame.image.fromstring(raw_data, size, "RGB")
        self.screen.blit(surf, (0, vertical_offset))

    def __draw_data(self, data, override_info: str = None):
        graphs, predicted_prob_dist, true_class, predicted_class, classification_accuracy, (time_window, window_from, window_to), (
        confusion_matrix, keep_activities) = data
        self.__draw_graphs(graphs)
        self.__draw_probability_dist(predicted_prob_dist)
        self.__draw_real_label(true_class)
        self.__draw_sensor_info()
        self.__draw_predicted_label(predicted_class)
        self.__draw_classification_accuracy(classification_accuracy)
        self.__draw_info(time_window)
        self.__draw_timestamp(window_from, window_to, override_text=override_info)
        self.__draw_confusion_matrix(confusion_matrix, keep_activities)
        pygame.display.flip()

    def run_game_loop(self, results: iter):
        """
        Runs loop and displays next info in each iteration
        :param results: Iterable with displayed info in correct format
        """
        last_data = None
        running = True
        while running:
            self.screen.fill((255, 255, 255))
            data = next(results, None)
            if data:
                last_data = data
                self.__draw_data(data)
            else:
                self.__draw_data(last_data, override_info="Demo classification FINISHED")
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return

    
    
