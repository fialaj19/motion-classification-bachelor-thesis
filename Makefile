demo_hand_chest_ankle_CNN:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12,21,22,23,27,28,29,38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="hand,chest,ankle" --model=CNN
	
demo_hand_CNN:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12" --movement-column=1 -d=" " --sensors="hand" --model=CNN
	
demo_ankle_CNN:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="ankle" --model=CNN

demo_chest_CNN:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="21,22,23,27,28,29" --movement-column=1 -d=" " --sensors="chest" --model=CNN
	
demo_hand_ankle_CNN:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12,38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="hand,ankle" --model=CNN
	
demo_hand_LSTM:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12" --movement-column=1 -d=" " --sensors="hand" --model=LSTM
	
demo_hand_chest_ankle_LSTM:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12,21,22,23,27,28,29,38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="hand,chest,ankle" --model=LSTM
	
demo_hand_ankle_LSTM:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="4,5,6,10,11,12,38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="hand,ankle" --model=LSTM
	
demo_ankle_LSTM:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="ankle" --model=LSTM
	
demo_chest_LSTM:
	cd src; \
	python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/$(subject).dat" --reordering="21,22,23,27,28,29" --movement-column=1 -d=" " --sensors="chest" --model=LSTM
	
install_requirements:
	sudo apt-get update && \
	sudo apt install python3-pip && \
	cd src && \
	pip install -r requirements.txt
	
train_all_models:
	cd src; \
	./train_all_models.bash
	
