# Softwarová aplikace pro real time klasifikaci pohybu
Softwarová aplikace pro real time klasifikaci pohybu založená na umělé inteligenci

Autor: Jan Fiala

# Struktura práce
- `src` obsahuje zdrojový kód práce
- `trained_models` obsahuje natrénované modely pro různé kombinace parametrů
- `demo_data` obsahuje záznamy pohybu dvou subjektů. Tyto subjekty se nevyskytují v datové sadě použité pro trénink modelů
- `preprocessed_datasets` obsahuje předzpracovaný dataset podle různých parametrů. Slouží k urychlení učení více modelů
- `training_data` obsahuje dataset PAMAP2 použitý v této práci

# Spuštění
Pro spuštění práce je nutný operační systém typu linux/unix. Zdrojový kód byl vyvíjen a testován na operačním systému ubuntu.

Práci lze spouštět pouze pomocí Makefile nebo python skriptů.

Nejdříve je nutné nainstaloval použité Python balíčky pomocí příkazu `make install_requirements`
Poté lze spustit přiložené demo nebo klasifikaci s vlastními parametry.

Pokud spouštíte přímo soubory run_demo.py nebo run_training.py je nutné je spouštět ze složky `src`

Práce podporuje Python 3.8 a výše

## Makefile
Soubor Makefile nabízí jednoduché spuštění aplikace pomocí následujících příkazů, kde je nutné nahradit SUBJEKT za název subjektu. Přiložená data obsahují subjekty: "subject103" a "subject106"

- `make install_requirements` nainstaluje balíčky nutné pro spuštění práce
- `make subject=SUBJEKT demo_hand_chest_ankle_CNN` spustí demo klasifikaci při použití senzorů na zápěstí, hrudníku a kotníku s použítím konvoluční neuronové sítě. 
- `make subject=SUBJEKT demo_hand_ankle_CNN` spustí demo klasifikaci při použití senzorů na zápěstí a kotníku s použítím konvoluční neuronové sítě. 
- `make subject=SUBJEKT demo_hand_CNN` spustí demo klasifikaci při použití senzoru pouze na zápěstí s použitím konvoluční neuronové sítě
- `make subject=SUBJEKT demo_chest_CNN` spustí demo klasifikaci při použití senzoru pouze na hrudníku s použitím konvoluční neuronové sítě
- `make subject=SUBJEKT demo_ankle_CNN` spustí demo klasifikaci při použití senzoru pouze na kotníku s použitím konvoluční neuronové sítě
- `make subject=SUBJEKT demo_hand_chest_ankle_LSTM` spustí demo klasifikaci při použití senzorů na zápěstí, hrudníku a kotníku s použítím LSTM sítě. 
- `make subject=SUBJEKT demo_hand_ankle_LSTM` spustí demo klasifikaci při použití senzorů na zápěstí a kotníku s použítím LSTM sítě. 
- `make subject=SUBJEKT demo_hand_LSTM` spustí demo klasifikaci při použití senzoru pouze na zápěstí s použitím LSTM sítě
- `make subject=SUBJEKT demo_chest_LSTM` spustí demo klasifikaci při použití senzoru pouze na hrudníku s použitím LSTM sítě
- `make subject=SUBJEKT demo_ankle_LSTM` spustí demo klasifikaci při použití senzoru pouze na kotníku s použitím LSTM sítě
- `make train_all_models` spustí trénink modelů pro různé kombinace parametrů. Není nutné spouštět, protože práce už obsahuje tyto natrénované modely.

Pro spuštění demo klasifikace pomocí make je nutné nahradit SUBJEKT za název subjektu. Přiložená data obsahují subjekty: "subject103" a "subject106". Příkladem špuštění grafického dema je příkaz:
`make subject=subject106 demo_hand_chest_ankle_CNN`

## run_demo.py
Grafické demo lze spouštět i přímo pomocí skriptu `run_demo.py`. Vstupní soubor musí obsahovat záznam ze senzorů ve formátu CSV s frekvencí 100Hz. Tento skript přijímá následující parametry:

- `--input-file` cesta k souboru obsahující vstupní data pro demo. Data musí být ve formátu CSV a záznam musí být pořízen senzory s frekvencí 100Hz.
- `--time-window` délka časového okna (v sekundách), pro kterou musí existovat již natrénovaný model. Podle této délky bude také rozdělen vstupní soubor.
- `--overlap` velikost překryvu časových oken. Povolené hodnoty jsou od 0.0 (žádný překryv) do 1.0 (kromě - 100% překryv)
- `--sensors` umístění jednotlivých senzorů. Podporovaná umístění jsou: hand, chest, ankle. Lze použít i více senzorů najednou, pro oddělení použijte čárku.
- `--reordering` použité sloupce ze vstupního souboru. Pro každý senzor z předchozího argumentu je nutné uvést právě 6 sloupců v pořadí (nejdříve 3 sloupce pro osy x,y,z z akcelerometru a poté 3 sloupce pro x,y,z z gyroskopu)
- `--movement-column` číslo sloupce s prováděným typem pohybu. Slouží pro vyhodnocení kvality klasifikátoru
- `--input-file-delimiter` znak oddělující sloupce v každém řádku vstupního souboru
- `--model` typ použitého klasifikátoru. Podporované typy: LSTM (Obousměrná LSTM sít´) a CNN (Konvoluční neuronová sít´)

Dále, pokud se rozhodnete použít přiložená demo data pro spuštění demo-klasifikace, tak čísla sloupců odpovídají senzorům následovně (každé trojčíslí odpovídá osám x, y a z v tomto pořadí daného senzoru):
- 4,5,6 odpovídá akcelerometru ze senzoru zápěstí
- 10,11,12 odpovídá gyroskopu ze senzoru zápěstí
- 21,22,23 odpovídá akcelerometru na hrudníku
- 27,28,29 odpovídá gyroskopu na hrudníku
- 38,39,40 odpovídá akcelerometru na kotníku
- 44,45,46 odpovídá gyroskopu na kotníku

Příklad spuštění grafického dema z konzole:
`python3 run_demo.py --time-window=10 --overlap=0.5 --input-file="../demo_data/subject106.dat" --reordering="4,5,6,10,11,12,21,22,23,27,28,29,38,39,40,44,45,46" --movement-column=1 -d=" " --sensors="hand,chest,ankle" --model=CNN`

## run_training.py
Trénink modelu lze spustit pomocí souboru `run_training.py`

Tento skript přijímá následující parametry:

- `--time-window` délka časového okna (v sekundách), pro kterou se natrénuje model.
- `--overlap` velikost překryvu časových oken. Povolené hodnoty jsou od 0.0 (žádný překryv) do 1.0 (kromě - 100% překryv). Větší překryv také uměle zvětší trénovací dataset.
- `--sensors` umístění jednotlivých senzorů. Podporovaná umístění jsou: hand, chest, ankle. Lze použít i více senzorů najednou, pro oddělení použijte čárku.
- `--model` typ použitého klasifikátoru. Podporované typy: LSTM (Obousměrná LSTM sít´) a CNN (Konvoluční neuronová sít´)
- `--epochs` počet epoch tréninku klasifikátoru
- `--batch-size` velikost trénovací dávky dat. Sníží využití operační paměti za možné snížení klasifikační přesnosti

Příklad spuštění tréninku z konzole:
`python3 run_training.py --time-window=10 --model=CNN --overlap=0.5 --sensors="hand,chest,ankle" --batch-size=300 --epochs=50`

## Natrénované modely
Práce obsahuje natrénované klasifikační modely, které lze spouštet pomocí `run_demo.py` popsaného výše.

Tyto modely byly natrénovány pro všechny kombinace následujících parametrů:
- `--model` "LSTM", "CNN"
- `--time-window` 5, 10, 20, 30
- `--overlap` 0.0, 0.3, 0.5
- `--sensors` "hand", "chest", "ankle", "hand,chest", "hand,ankle", "chest,ankle", "hand,chest,ankle"


